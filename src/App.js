import "./App.css";
import Navigation from "./Components/Navigation";
import { Switch, Route } from "react-router-dom";
import HomePage from "./Pages/HomePage";
import { useSelector } from "react-redux";
import { selectAppLoading } from "./store/appStatus/selectors";
import LoadingSpinner from "./Components/LoadingSpinner";

function App() {
  const appLoading = useSelector(selectAppLoading);

  return (
    <div className="App">
      <Navigation />
      <Switch>
        {appLoading ? <LoadingSpinner /> : null}
        <Route path="/" component={HomePage} exact />
      </Switch>
    </div>
  );
}

export default App;
