import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

import "bootstrap/dist/css/bootstrap.min.css";

export default function Navigation() {
  return (
    <Navbar
      style={{
        fontSize: 30,
        fontWeight: "bold",
        padding: "10px",
        backgroundColor: "#fff",
      }}
      expand="lg"
    >
      <Navbar.Brand to="/">
        <img
          src="https://uploads-ssl.webflow.com/5ac150ab7e9bbe927474fbde/5f5a65119f2ea44d8086ce9c_Envision%20Logo%20V2.png"
          width={200}
          height={38}
          className="d-inline-block align-top"
          alt="envision logo"
          style={{
            margin: "5px",
          }}
        />
      </Navbar.Brand>
      <Navbar.Toggle
        style={{ border: "0px" }}
        aria-controls="basic-navbar-nav"
      />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav style={{ width: "100%" }} fill></Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
