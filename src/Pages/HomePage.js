import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { extractText } from "../store/imgOutput/actions";
import { selectFileName, selectResult } from "../store/imgOutput/selectors";
import { Container, Row, Col } from "react-bootstrap";

import { IoIosPlayCircle } from "react-icons/io";
import { IoIosRefreshCircle } from "react-icons/io";

import "./HomePage.css";

function HomePage() {
  const result = useSelector(selectResult);
  const fileName = useSelector(selectFileName);

  const dispatch = useDispatch();
  const [image, setImage] = useState();

  return (
    <Container>
      {!result ? (
        <div>
          <Row>
            <Col>
              <div>
                <p>Upload an image and get the output of the text.</p>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <hr className="separationLine" />
            </Col>
          </Row>
          {!image ? (
            <div>
              <Row>
                <Col>
                  <p style={{ fontWeight: "bold" }}>Upload an image</p>
                </Col>
              </Row>
              <Row>
                <Col>
                  <input
                    className="file-input"
                    type="file"
                    onChange={(e) => {
                      setImage(e.target.files[0]);
                    }}
                  ></input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <small>No file selected</small>
                </Col>
              </Row>
            </div>
          ) : (
            <div>
              <Row>
                <Col>
                  <p style={{ fontWeight: "bold" }}>
                    1 file added successfully!
                  </p>
                </Col>
              </Row>
              <Row>
                <Col>
                  <button
                    className="output-button"
                    onClick={(e) => {
                      dispatch(extractText(image));
                    }}
                  >
                    Display Output
                  </button>
                </Col>
              </Row>
              <Row>
                <Col>
                  <small>File Uploaded</small>
                </Col>
              </Row>
            </div>
          )}
          <Row>
            <Col>
              <hr className="separationLine" />
            </Col>
          </Row>
        </div>
      ) : (
        <Container>
          <Row>
            <Col>
              <h3>
                Output of <strong>{fileName}</strong>
              </h3>
            </Col>
          </Row>
          <Row>
            <Col className="resultBox">
              {result?.map((res) => {
                return <p key={res.paragraph}>{res.paragraph}</p>;
              })}
            </Col>
          </Row>
          <Row>
            <Col>
              <IoIosPlayCircle style={{ fontSize: "80px", color: "#31359b" }} />
              Play/Pause
            </Col>
            <Col>
              <IoIosRefreshCircle
                style={{ fontSize: "80px", color: "#31359b" }}
              />
              Resume
            </Col>
          </Row>
        </Container>
      )}
    </Container>
  );
}

export default HomePage;
