import axios from "axios";
import { appLoading, appDoneLoading } from "../appStatus/actions";

export const setResult = (result) => {
  return {
    type: "set_result",
    payload: result,
  };
};
export const setFileName = (selectedFile) => {
  return {
    type: "set_imgName",
    payload: selectedFile,
  };
};

export const extractText = (selectedFile) => {
  return async (dispatch, getState) => {
    dispatch(appLoading());
    const formData = new FormData();
    formData.append("photo", selectedFile);
    console.log(selectedFile);

    try {
      const response = await axios.post("/api/test/readDocument", formData);
      console.log("response", response);
      const data = response.data;
      console.log(data);

      if (!data) {
        dispatch(setResult(null));
        dispatch(setFileName(null));
        dispatch(appDoneLoading());
        return;
      }

      dispatch(setFileName(selectedFile.name));
      dispatch(setResult(data.response.paragraphs));
      dispatch(appDoneLoading());
      return;
    } catch (error) {
      console.log("Error!", error.message);
    }
  };
};
