const initialState = {
  result: null,
  fileName: null,
};

const imgReducer = (state = initialState, action) => {
  switch (action.type) {
    case "set_result":
      return { ...state, result: action.payload };
    case "set_imgName":
      return { ...state, fileName: action.payload };
    default:
      return state;
  }
};

export default imgReducer;
