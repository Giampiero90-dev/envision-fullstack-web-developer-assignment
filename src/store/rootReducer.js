import { combineReducers } from "redux";
import appStatus from "./appStatus/reducer";
import imgReducer from "./imgOutput/reducer";

export default combineReducers({
  appStatus,
  imgReducer,
});
